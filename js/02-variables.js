'use strict' // buena practica de programcion

//Variables: es un contenedor de informacion
//let: permite definir variables para limitar el bloque
//var define una variable local o global sin importar donde este


var pais = 'Espana';
var continente = "Europa";
var antiguedad = 2019;
var pais_y_continente = pais + ' ' + continente;
let prueba = "hola";
alert("hola");


pais = "Mexico";
continente = "latinoamerica";
//var pais_y_continente = pais + ' ' + continente;

console.log(pais, continente, antiguedad);
alert(pais_y_continente);