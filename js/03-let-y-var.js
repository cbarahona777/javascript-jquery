'use strict'
// pruebas con let y var

var numero = 40;
console.log(numero); // valor 40

if (true) {
    numero = 50;
    console.log(numero); // valor 50

}

console.log(numero); // valor 50


//prueba con let

var texto = "Curso de js";
console.log(texto); // pone el js

if (true) {
    let texto = "curso laravel 5";
    console.log(texto); //valor laravel
}

console.log(texto); // valor js