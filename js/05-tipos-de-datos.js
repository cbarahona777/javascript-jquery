'use strict'

//operadores
var numero1 = 7;
var numero2 = 12;
var suma = numero1 + numero2;
var resta = numero1 - numero2;
var multi = numero1 * numero2;
var div = numero1 / numero2;

// alert("El resultado de la suma es : " + suma);
// alert("El resultado de la resta es : " + resta);
// alert("El resultado de la multiplicacion es : " + multi);
// alert("El resultado de la division es : " + div);

//tipos de datos
var numero_entero = 44;
var cadena_texto = "hola que tal";
var verdadero_o_falso = true;

var numero_falso = "33";

//console.log(Number(numero_falso) + 7);
console.log(parseInt(numero_falso) + 7);

//typeof();