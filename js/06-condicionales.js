'use strict'

//condicionales
//IF : si A es igual a B entonces haz algo

// var edad1 = 10;
// var edad2 = 12;

// //si pasa esto 
// if (edad1 > edad2) {
//     //ejecuta esto
//     console.log("edad 1 es mayor que edad 2");

// } else {
//     console.log("la edad no es inferior");
// }

var edad = 80;
var nombre = 'David Suarez';
/* operadores relacionales

mayor: >
menor: <
mayor o igual: >=
menor o igual: <=
distinto: !=
igual: ==
*/

if (edad >= 18) {
    // es mayor de edad
    console.log(nombre + " tiene " + edad + " anios, es mayor de edad");

    if (edad <= 34) {
        console.log('todavia eres milenial');
    } else if (edad >= 70) {
        console.log('eres anciano');
    } else {
        console.log('ya no eres milenial');
    }
} else {
    //es menor de edad
    console.log(nombre + " tiene " + edad + " anios, es menor de edad");
}


//operadores logicos

/*
AND(Y): &&
OR(o): ||
NEGACION: !

*/
var year = 2028;
//Negacion
if (year != 2016) {
    console.log("el anio no es 2016 realmente es " + year);
}

//AND
if (year >= 2000 && year <= 2020) {
    console.log("estamos en la era actual");
} else {
    console.log("estamos en la era post moderna");
}

//OR
if (year == 2008 || (year >= 2018 && year == 2028)) {
    console.log("el anio acaba en 8");
} else {
    console.log("anio no registrado");
}