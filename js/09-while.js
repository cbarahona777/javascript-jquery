'use strict'

//bucle while

var year = 2018;

while (year != 1991) {
    //ejecuta esto
    console.log("estamos en el anio " + year);

    if (year == 2000) {
        break;
    }

    year--;


}

//DO while
var years = 20;
do {

    alert("solo cuando sea diferente a 20");
    years = 20;
} while (years != 20)