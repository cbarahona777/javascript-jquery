'use strict'

//funciones
//una fx es una agrupacion reutilizable de un conjunto de instrucciones que podemos utilizar cuantas veces queramos
// var x = parseInt(prompt("ingrese el primer numero"));

// var y = parseInt(prompt("ingrese el segundo numero"));
function porConsola(x, y) {
    console.log("suma: " + (x + y));
    console.log("resta: " + (x - y));
    console.log("multiplicacion: " + (x * y));
    console.log("division: " + (x / y));
    console.log("***********************************");
}

function porPantalla(x, y) {
    document.write("suma: " + (x + y) + "<br/>");
    document.write("resta: " + (x - y) + "<br/>");
    document.write("multiplicacion: " + (x * y) + "<br/>");
    document.write("division: " + (x / y) + "<br/>");
    document.write("***********************************");
}


function calculadora(x, y, mostrar = false) {
    //conjunto de instrucciones a ejecutar

    if (mostrar == false) {
        porConsola(x, y);
    } else {
        porPantalla(x, y);
    }
    return true;
}

//invocar a la fx
//calculadora();
calculadora(10, 40);
calculadora(11, 4, true);
calculadora(10, 14, true);

// var resultado = calculadora();
// console.log(resultado);
/*
calculadora(10, 11);
calculadora(5, 7);


for (var i = 1; i <= 10; i++) {
    console.log(i);
    calculadora(i, 8);

}*/